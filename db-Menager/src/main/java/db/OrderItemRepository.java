
package db;

import java.util.List;
import model.OrderItem;

public interface OrderItemRepository{
   
    public List<OrderItem> withDescription (String description, PagingInfo page);
    public List<OrderItem> withPrice(Double price, PagingInfo page);
    public List <OrderItem> withId(int id);
    public List<OrderItem> allOnPage(PagingInfo page);
    public void add(List <OrderItem> orderItem);
    public void modify(List <OrderItem> orderItem);
    public void remove(OrderItem orderItem);
}
