/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.catalog;

import db.AdressRepository;
import db.ClientRepository;
import db.OrderItemRepository;
import db.OrderRepository;
import db.repository.hsqlAdressRespository;
import db.repository.hsqlClientRepository;
import db.repository.hsqlOrderItemRepository;
import db.repository.hsqlOrderRepository;

import java.sql.Connection;

public class hsqlRepositoryCatalog implements RepositoryCatalog {
    Connection connection;

    public hsqlRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }
	
    public OrderRepository order() {
	return new hsqlOrderRepository(connection);
    }
    
    public AdressRepository adress(){
        
        return new hsqlAdressRespository(connection);
    }
    public  ClientRepository client (){
        return new hsqlClientRepository(connection);
    }
    public OrderItemRepository orderItem (){
        return new hsqlOrderItemRepository(connection);
    }
}

