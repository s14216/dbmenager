/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.repository.UnitOfWork;

import model.Entity;
public interface IUnitofWorkRepository {
    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
