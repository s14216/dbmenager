
package db.repository;


import db.OrderRepository;
import db.PagingInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import model.Order;

public class hsqlOrderRepository implements OrderRepository {

Connection connection;
hsqlClientRepository hsqlClientRepository;
hsqlOrderItemRepository hsqlOrderItemRepository;
hsqlAdressRespository hsqlAdressRepository;



private String selectSql ="SELECT * FROM Order LIMIT(?,?,?)";
private String insertSql ="INSERT INTO Order(idClient,idItems,idDelivery) VALUES(?,?,?)";
private String selectByIdSql = "SELECT * FROM Order WHERE id=? LImit(?,?)";
private String updateSql = "UPDATE Order SET (idClient, idItems, idDelivery)=(?,?,?) WHERE id=?";
private String deleteSql = "DELETE FROM Order WHERE id=?";

private PreparedStatement select;
private PreparedStatement delete;
private PreparedStatement insert;
private PreparedStatement selectById;
private PreparedStatement update;

private String createOrderTable =""
			+ "CREATE TABLE Order("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "idClient LONG,"
                        + "idItems LONG"
                        + "idDelivery LONG,"
			+ ")";


    public hsqlOrderRepository(Connection connection) {
        this.connection = connection;
        try{
			

			
			
			select = connection.prepareStatement(selectSql);
                        insert = connection.prepareStatement(insertSql);
                        selectById = connection.prepareStatement(selectByIdSql);
                        update = connection.prepareStatement(updateSql);
                        delete = connection.prepareStatement(deleteSql);
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			
			boolean tableExists =false;
			while(rs.next())
			{
				if(rs.getString("TABLE_NAME").equalsIgnoreCase("Order")){
					tableExists=true;
					break;
				}
			}
			if(!tableExists){
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(createOrderTable);
			}
		}catch(SQLException ex){
			ex.printStackTrace();
		}
        	
	}
     
      public void add(Order order) {
		try {
                        hsqlClientRepository.add(order.getClientDeatils());
                        insert.setLong (1, order.getClientDeatils().getId());
			hsqlOrderItemRepository.add(order.getItems());
                        hsqlAdressRepository.add(order.getDeliveryAdress());
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
      
       public void modify(Order order) {
		try {
			hsqlClientRepository.modify(order.getClientDeatils());
                        hsqlAdressRepository.modify(order.getDeliveryAdress());
                        hsqlOrderItemRepository.modify(order.getItems());
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
      
       public List<Order> allOnPage(PagingInfo page){
          List<Order> result = new ArrayList<Order>();
          int i = 0;
             try {
			
			ResultSet rs = select.executeQuery();
			while(rs.next()){
                                result.add(withId(i));
                                i++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
           
      
     
      
      public Order withId (int id){
		Order result = null;
		try {
			selectById.setInt(1, id);
			ResultSet rs = selectById.executeQuery();
			while(rs.next()){
				Order order = new Order ();
				order.setClientDeatils(hsqlClientRepository.withId(id));
				order.setDeliveryAdress(hsqlAdressRepository.withId(id));
				order.setItems(hsqlOrderItemRepository.withId(id));
				result = order;
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

  

    public void remove(Order order) {
        try {
			
                        delete.setLong(1, order.getId());
                        hsqlClientRepository.remove(order.getClientDeatils());
                        hsqlAdressRepository.remove(order.getDeliveryAdress());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }


    
      
      
      
     
     

     
      

}
    
  



