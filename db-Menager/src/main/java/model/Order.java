
package model;


import java.util.ArrayList;

public class Order {

    private long id;
    private Client clientDeatils;
    private Adress deliveryAdress;
    private ArrayList<OrderItem>items = new ArrayList<OrderItem>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Client getClientDeatils() {
        return clientDeatils;
    }

    public void setClientDeatils(Client clientDeatils) {
        this.clientDeatils = clientDeatils;
    }

    public Adress getDeliveryAdress() {
        return deliveryAdress;
    }

    public void setDeliveryAdress(Adress deliveryAdress) {
        this.deliveryAdress = deliveryAdress;
    }

    public ArrayList<OrderItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<OrderItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", clientDeatils=" + clientDeatils + ", deliveryAdress=" + deliveryAdress + ", items=" + items + '}';
    }
    
    
}


